# iSTPManger
---


## Installation
### iSTP-*-Service
1. OS

  Linux Mint 17.1


2. Runtime & Third party
    * Build tools
    
        ```bash
        $ sudo apt-get install build-essential
        ```
    * Node.js `0.10.38`
    
        ```bash
        mkdir devlib
        cd devlib  
        wget http://nodejs.org/dist/v0.10.38/node-v0.10.38.tar.gz  
        tar xzf node-v0.10.38.tar.gz  
        cd node-v0.10.38
        ./configure  
        make  (use -jN as per number of cores in order to speed up)
        sudo make install
        ```
    * node-gyp
    
        ```bash
        $ npm install -g node-gyp
        ```
    * bower
    
        ```bash
        $ npm install -g bower
        ```
    * libavahi-compat-libdnssd-dev

        ```bash
        $ apt-get install libavahi-compact-libdnssd-dev
        ```
    * Mosquitto
    
        ```bash
        wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
        sudo apt-key add mosquitto-repo.gpg.key
        cd /etc/apt/sources.list.d/
        sudo wget http://repo.mosquitto.org/debian/mosquitto-wheezy.list
        sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
        apt-get update
        apt-get install mosquitto
        ```

    > **注意:** 安装第三方环境时需有互联网环境。

3. Dependencies

    解包应用程序。找到`package.json`文件并且切换当前目录与之相同运行下面命令。
    
    ```bash
    $ npm install
    ```
    > **注意:** 运行上面命令时需有互联网环境。


4. Configurations
    1. __data\config.json__
        * __istp-log-service__ 部分。
            * __silent__: {boolean} [false] 是否输入日志，为假有日志输出，否则无日志输出。
            * __level__: {string | silly, debug, verbose, info, warn, error} [info] 日志输出级别。
            * __destination__: {array(string) | console, file, db} [console] 日志存储方式。
            * __timestamp__: {boolean} [false] 是否显示时间戳。
            * __showname__: {boolean} [true] 是否显示日志来源。
            * __ip__: {string} [0.0.0.0] 日志服务绑定地址。
            * __port__: {integer} [9000] 日志服务绑定端口。
        * __istp-ui-service__ 部分。
        __istp-ui-service__ Web UI 服务配置。
            * __ip__: {string} [0.0.0.0] Web服务绑定本地地址。
            * __port__: {integer} [10080] Web服务绑定端口。
            * __apppath__: {string} [/public] Web应用路径。
        * __logger__:
            * __name__: {string} [Logger] 日志名称，在日志服务器端会分类。
            * __level__: {string | silly, debug, verbose, info, warn, error} [info] 日志输出级别。
            * __silent__: {boolean} [false] 是否输入日志，为假有日志输出，否则无日志输出。
            * __destination__: {string | console, udp} [console] 日志输出方式。
            * __ip__: {string} [127.0.0.1] 日志服务器地址。
            * __port__: {integer} [10514] 日志服务器端口。

            > __ip__，__port__仅在__destination__为__udp__时生效。
