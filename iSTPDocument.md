# iSTP情况介绍

## 1. iSTP项目中开发的应用

* TLE2XML

	开发工具及语言：Visual Studio，C#

	运行环境： Windows，.netframework

	数据转换工具，将公司TLE数据转换为iSTP系统定义的数据格式，可生成轨旁和TSAdapter使用的xml数据，生成站场图应用使用的svg数据，生成应答器列表。

	> ***如无其他数据方面变化，无需修改此工具。***


* iSTP bundle

	开发工具及语言：O3S Studio，C++

	运行环境：Linux，O3S

	轨旁消息转换接口，用于将外部发送给轨旁仿真的消息转换成对象供轨旁仿真使用，用于将轨旁仿真提供的对象序列化后发给外部。

	> ***如不增加其他消息，无需修改此工具。***


* iSTPManager

	开发工具及语言：Python，node.js，JavaScript，C++，Windows shell，bash shell，AutoIT

	运行环境：Windows，Linux

	平台管理系统，目前采用了[BS架构](https://zh.wikipedia.org/zh/%E6%B5%8F%E8%A7%88%E5%99%A8-%E6%9C%8D%E5%8A%A1%E5%99%A8)，包括站场管理，设备管理，日志服务，自动测试执行；原计划还要做`测试场景的记录与回放`，`培训管理`，`测试管理`等系统，因时间与能力有限没能进行研究，希望后续工作的同事能进一步完善。

	之前系统之间通信采用了通过Zeroconf协议发现服务并利用Websocket协议进行通信的方式。后期可改为以Mosquitto为中心的订阅发布通信模式，目前`站场管理`功能已改为此模式。


* IOPGW

	开发工具及语言：Visual Studio，C#

	运行环境：Windows，.netframework

	城际互联互通测试网关。

	> 开发依据文档：[CJTL_实验室互联互通网关通信协议(V0.1.5).doc](./attachments/CJTL_实验室互联互通网关通信协议(V0.1.5).doc)
	>
	> ***进行互联互通测试时使用，如依据文档未变化，无需修改此网关程序。***

* iSTPSatellite

	开发工具及语言：Visual Studio，C#

	运行环境：Windows，.netframework

	为自动化测试能操作BIOS开发的代替人工操作BIOS工具。后期如有操作CTC的需求可在其中开发操作CTC的功能。

* telegram

	开发工具及语言：C++，node-gyp

	运行环境：Linux，node.js

	基于node.js开发的add-on工具，完成报文转换，支持报文的扰码和解扰码，支持将报文解成可供人阅读的包和字段的形式，支持将JSON形式的报文转换为128字节应答器报文。

	> ***常见报文均已验证***


----


## 2. 运行环境及运行时

* [node.js](https://nodejs.org/)

    > version: 0.10.x for Linux

* [Python](https://www.python.org/)

    > version: 2.7.x for Windows & Linux

* [O3S](http://www.opentekhnia.com/)

	> for Linux

* [.netframework](https://www.microsoft.com/net)

	> for Windows
* [Bonjour](https://www.apple.com/support/bonjour/)

	> for Windows

* [Chrome Browser](https://www.google.com/chrome/)

	> Last version for Windows & Linux

* [Mosquitto](http://mosquitto.org/)

	> Last version for Linux


## 3. 用到的概念，第三方项目及开源项目

* [node-gyp](https://github.com/nodejs/node-gyp)

	nodejs add-on编译工具

* [nvm](https://github.com/creationix/nvm)

	nodejs 版本管理工具

* [npm](https://www.npmjs.com/)

	nodejs包管理工具

* [gulp](http://gulpjs.com/)

	自动化构建工具，在新的iSTPWeb开发中利用该工具写了一个自动化构建脚本。当源文件被修改并保存后，自动按照部署到发布目录中，并自动刷新浏览器，查看修改后的结果。
	gulp自动化构建脚本所在目录`iSTPManager\public\gulpfile.js`。

* [Polymer](https://www.polymer-project.org/)

	Google公司最新的创建HTML Elements的库。利用Polymer可方便的建立HTML Elements库，并用创建的HTML Elements库创建应用。局部修改不需要修改整个应用，只需修改相应的HTML Element。改善了以往Web应用难以维护修改的状况。

	此外，Polymer提供了[Google Material Design](https://elements.polymer-project.org/browse?package=paper-elements)风格的HTML Elements，可直接用于项目中Elements的开发。

* [Mosquitto](http://mosquitto.org/)

	基于[MQTT](http://mqtt.org/)协议的中间件。

* [Bower](http://bower.io/)

	Web站点的包管理工具。

* [mdns](https://www.npmjs.com/package/mdns)

	项目中使用到的[zeroconf](http://www.zeroconf.org/)的nodejs包。

* [Socket.IO](http://socket.io/)

	项目中用到的[Websocket](http://www.websocket.org/)的nodejs包。

* [mqtt](https://www.npmjs.com/package/mqtt)

	项目中用到的[MQTT](http://mqtt.org/)的nodejs包。

* [SVG](https://www.w3.org/Graphics/SVG/)

	SVG是W3C标准，是一种用于网络的基于矢量的图形，使用XML格式定义了图形。

* [requirejs](http://requirejs.org/)

	RequireJS的目标是鼓励代码的模块化，它使用了不同于传统\<script\>标签的脚本加载步骤。可以用它来加速、优化代码，但其主要目的还是为了代码的模块化。

* [D3.js](https://d3js.org/)

	D3是一个用于在HTML，SVG和CSS可视化的JavaScript库。

* [jQuery](https://jquery.com/)

	JavaScript函数库。

* [JSON](http://www.json.org/)

	JSON是一种轻量级的数据交换格式，以文字为基础，且易于让人阅读。Javascript原生支持。

* [WoL](https://en.wikipedia.org/wiki/Wake-on-LAN)

